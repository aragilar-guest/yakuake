yakuake (22.04.1-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (22.04.1).
  * Drop patches applied upstream.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 28 May 2022 10:16:01 +0200

yakuake (21.12.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (21.12.3).
  * Cherry-pick upstream patch to fix slide effect on Wayland.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 06 Mar 2022 17:12:52 +0100

yakuake (21.12.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update standards version to 4.6.0, no changes needed.

 -- Pino Toscano <pino@debian.org>  Sun, 27 Feb 2022 12:16:37 +0100

yakuake (21.08.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (21.08.0).

 -- Norbert Preining <norbert@preining.info>  Mon, 16 Aug 2021 16:42:30 +0900

yakuake (21.04.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (21.04.0).
  * Borrow minimal upstream signing key from k3b.
  * Added myself to the uploaders.

 -- Norbert Preining <norbert@preining.info>  Tue, 27 Apr 2021 13:57:56 +0900

yakuake (20.12.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Change the maintainer to Debian Qt/KDE Maintainers, as it is no more an
    "extra" application.
  * Fix day-of-week for changelog entry 2.7.5-1.
  * Unregister the old /etc/xdg/yakuake.knsrc conffile.

 -- Pino Toscano <pino@debian.org>  Thu, 07 Jan 2021 21:30:04 +0100

yakuake (20.12.0-1) unstable; urgency=medium

  * Refresh upstream metadata, remove obsolete fields Contact, Name already
    present in machine-readable copyright file, add Bug-Browse.
  * New upstream release (20.12.0).
  * Bump Standards-Version to 4.5.1, no changes required.
  * Update project homepage.

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 10 Dec 2020 23:52:55 +0100

yakuake (20.08.3-1) unstable; urgency=medium

  * Refresh copyright information.
  * New upstream release (20.08.3).

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 09 Nov 2020 11:10:17 +0100

yakuake (20.08.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - bump KF packages to 5.60.0
  * Update the patches:
    - fix-upstream-version-in-about-dialog: drop, fixed upstream

 -- Pino Toscano <pino@debian.org>  Fri, 14 Aug 2020 00:08:16 +0200

yakuake (20.04.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Thu, 30 Jul 2020 23:26:18 +0200

yakuake (20.04.0-1) unstable; urgency=medium

  * New upstream release (20.04.0):
    - Tab splits now start in the same directory as the current tab/split.
    - You can resize Yakuake’s window vertically by dragging on its bottom
      bar.
  * Build with hardening=+all build hardening flag.
  * Refresh patch to show correct upstream version in about dialog.
  * Refresh copyright information.
  * Remove explicit --as-needed from linker flags, it’s now injected by
    default.
  * Use pkg-kde-extras@alioth-lists.debian.net as maintainer email in place of
    the deprecated alioth list address.
  * Bump Standards-Version to 4.5.0, no changes required.
  * Bump debhelper compat level to 13.
  * Add upstream bug submit URL to upstream metadata.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 01 May 2020 00:14:09 +0200

yakuake (19.12.2-1) unstable; urgency=medium

  * New upstream release (19.12.2).
  * Refresh patch to fix version in about dialog.

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 06 Feb 2020 18:06:16 +0100

yakuake (19.12.1-1) unstable; urgency=medium

  * Team upload.

  [ Aurélien COUDERC ]
  * New upstream release (19.12.1):
    - Better support for high DPI screens.
  * Add Rules-Requires-Root: no to debian/control.
  * Use debhelper-compat build dependency and remove debian/compat file.
  * Add myself to the list of uploaders.
  * Add patch to display correct application version in about dialog.
  * Lintian override for binary without manpage, yakuake doesn’t have command
    line arguments.
  * Update upstream project homepage URL.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 18 Jan 2020 09:21:16 +0100

yakuake (19.08.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update Vcs-* fields.
  * Drop the man page, outdated, and not providing any real value.
  * Explicitly add the gettext build dependency.
  * Bump Standards-Version to 4.4.1, no changes required.

 -- Pino Toscano <pino@debian.org>  Thu, 17 Oct 2019 13:34:40 +0200

yakuake (19.08.0-1) unstable; urgency=medium

  * Update watch file / add upstream signing key.
  * New upstream release.
  * Replace Lisandro with myself in uploaders.
  * Bump compat to 12.
  * Refresh copyright file to copyright-format 1.0.
  * Update build dependencies per cmake.
  * Bump Standards version 4.4.0; no remaining changes.
  * Update Vcs entries.
  * Add upstream/metadata.
  * Add hardening +bindnow.
  * Add lintian override for README outside /usr/share/doc as
    it describes the contents of the folder.
  * Release to unstable.

 -- Scarlett Moore <sgmoore@kde.org>  Sat, 24 Aug 2019 05:51:04 -0700

yakuake (3.0.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump the debhelper compatibility to 11:
    - bump the debhelper build dependency to 11~
    - bump compat to 11
    - remove --parallel for dh, as now done by default
  * Switch Vcs-* fields to salsa.debian.org.
  * Bump Standards-Version to 4.1.3, no changes required.
  * Simplify watch file, and switch it to https.
  * Remove trailing whitespaces in changelog.
  * Link in as-needed mode.

 -- Pino Toscano <pino@debian.org>  Mon, 02 Apr 2018 09:21:49 +0200

yakuake (3.0.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the build dependencies:
    - add libkf5wayland-dev on Linux only
    - replace deprecated kio-dev with libkf5kio-dev
    - explicitly add libx11-dev
    - bump version of all frameworks to 5.29.0, needed at least for knewstuff
  * Bump Standards-Version to 4.1.0, no changes required.
  * Update Vcs-* fields.

 -- Pino Toscano <pino@debian.org>  Sun, 03 Sep 2017 23:39:10 +0200

yakuake (3.0.2-1) unstable; urgency=medium

  [ Rohan Garg ]
  * New upstream release
  * Drop debian/docs which is legacy from the KDE 4 version.
  * Switch to using kf5 debhelper sequence
  * Switch to using konsole-kpart as a dependency

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Remove seemingly unneeded build dependencies. They might be pulled in
    by other B-Ds.
  * Update Standards-Version to 3.9.7, no changes required.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Fri, 04 Mar 2016 12:45:48 -0300

yakuake (2.9.9-3) unstable; urgency=medium

  * Put this package under the Debian KDE Extras Team's umbrella.
  * Add Vcs-Git and Vcs-Browser entries.
  * Update Vcs-Browser and Homepage fields with Diederik de Haas's patches.
    Thanks! (Closes: #813936).

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sun, 07 Feb 2016 00:00:41 -0300

yakuake (2.9.9-2) unstable; urgency=medium

  * Change maintainership from Ana to me. Thanks Ana *a lot* for all this years
    of providing us with yakuake!
  * Drop CDBS in favor of dh, because I prefer the later.
  * Switch to debhelper compat 9, I'm more used to it's behavior.
  * Do not use pkg-kde-tools meant for using inside the team.
  * Allow building the package in parallel.
  * Add libsoprano-dev as a build dependency, it used to get in by other
    build dependencies.
  * Depend upon the new konsole4-kpart instead of konsole (Closes: #798986).
  * Add a watch file.
  * Remove menu file, we already provide a desktop entry.
  * Bump Standards-Version to 3.9.6, no changes required.
  * Update debian/copyright.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Thu, 17 Sep 2015 22:27:44 -0300

yakuake (2.9.9-1) unstable; urgency=low

  * New upstream release:
   - Fixes: Yakuake does not show shell tabs when opening on a smaller screen
     using TwinView. (Closes: #523526)
  * Update minimum dependencies to KDE >= 4.7.1
  * Update to Standards-Version 3.9.4, no changes required.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Mon, 15 Jul 2013 00:14:52 +0200

yakuake (2.9.8-1) unstable; urgency=low

  * New upstream release.
  * Update to Standards-Version 3.9.1, no changes required.
  * Update minimum dependencies to KDE >= 4.3.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Sun, 06 Feb 2011 19:09:03 +0100

yakuake (2.9.7-1) unstable; urgency=low

  * New upstream release.
  * Switch to format "3.0 (quilt)", add source/format.
  * Update to Standards-Version 3.9.0, no changes required.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Sun, 18 Jul 2010 13:45:23 +0200

yakuake (2.9.6-1) unstable; urgency=low

  * New upstream release.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Tue, 19 May 2009 18:51:59 +0200

yakuake (2.9.5-1) unstable; urgency=low

  * New upstream release.
  * Update to Standards-Version 3.8.1, no changes required.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Mon, 11 May 2009 18:42:44 +0200

yakuake (2.9.4-2) unstable; urgency=low

  * Upload to unstable.
  * Package using pkg-kde-tools: add build depend on pkg-kde-tools and modify
    debian/rules accordingly.
  * Update build depends to >= 4.2.2.
  * Readd ${misc:Depends} lines.
  * Update copyright file.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Mon, 06 Apr 2009 20:23:52 +0200

yakuake (2.9.4-1) experimental; urgency=low

  * New upstream release.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Fri, 12 Sep 2008 17:21:33 +0200

yakuake (2.9.3-1) experimental; urgency=low

  * New upstream release.
  * Update to Standards-Version 3.8.0, no changes required.
  * Bump build-depends and depens to >= 4.1.0.
  * Include KDE4FAQ file in docs.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Wed, 03 Sep 2008 20:12:12 +0200

yakuake (2.9.2-1) experimental; urgency=low

  * New upstream release.
  * Bump build-depends and depens to >= 4.0.71.
  * Add build depend on libx11-dev, libxrender-dev.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Thu, 01 May 2008 15:57:50 +0200

yakuake (2.9-1) experimental; urgency=low

  * New upstream release.
  * Bump build-depends and depens to >= 4.0.2.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Mon, 10 Mar 2008 19:18:09 +0100

yakuake (2.9~beta1-1) experimental; urgency=low

  * New upstream release. Yakuake for KDE 4.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Thu, 17 Jan 2008 21:57:25 +0100

yakuake (2.8.1-1) unstable; urgency=low

  * New upstream release:
    - Fix non traslucent issues for non-KDE users. (Closes: 349144)
    - French translation is back. (Closes: #432091)
  * Move Homepage to source field.
  * Update to Standards-Version 3.7.3, no changes required.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Thu, 17 Jan 2008 21:54:40 +0100

yakuake (2.8-1) unstable; urgency=low

  * New upstream release.
  * Update menu file.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Mon, 01 Oct 2007 22:47:16 +0200

yakuake (2.8~beta1-1) unstable; urgency=low

   * The 'I like picnics indoors' release.
   * Pre-release of new upstream version. (Closes: #389108)
   * Drop patches. Relibtoolization in build time.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Mon, 07 May 2007 23:53:22 +0100

yakuake (2.7.5-4) unstable; urgency=low

  * Modified building system for using quilt.
  * Added space to homepage pseudo header.
  * Fixed .desktop file. (Closes: #389108)

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Sat, 28 Oct 2006 14:07:05 +0200

yakuake (2.7.5-3) unstable; urgency=low

  * Bumped compat to 5.
  * Bumped Standards-Version to 3.7.2, no changes required.
  * Removing spanish translation :( (Closes: #384929)

 -- Ana Beatriz Guerrero Lopez <ana@ekaia.org>  Sun, 10 Sep 2006 21:37:35 +0200

yakuake (2.7.5-2) unstable; urgency=medium

  * Substituting re-libtoolizing at build time with patches. (Closes: #379821)
  * Adding spanish translation from Juan Manuel Garcia Molina
    <juanma@debian.org> (Closes: #359244)
  * Added copyright of the debian packaging.

 -- Ana Beatriz Guerrero Lopez <ana@ekaia.org>  Thu, 27 Jul 2006 21:03:17 +0200

yakuake (2.7.5-1) unstable; urgency=low

  * New upstream release. Besides, yakuake has also a new upstream author.

 -- Ana Beatriz Guerrero Lopez <ana@ekaia.org>  Wed, 22 Mar 2006 22:43:07 +0100

yakuake (2.7.3-2) unstable; urgency=low

  * The package doesn't depend from libfreetype6 and other spurious
    libraries anymore.
  * Added dependency on konsole. (Closes: #347437)

 -- Ana Beatriz Guerrero Lopez <ana@ekaia.org>  Sat,  7 Jan 2006 18:43:27 +0100

yakuake (2.7.3-1) unstable; urgency=low

  * Initial release. (Closes: #336262)

 -- Ana Beatriz Guerrero Lopez <ana@ekaia.org>  Sat, 29 Oct 2005 11:08:43 +0000
